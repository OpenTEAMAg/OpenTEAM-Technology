# Paired Work Session Report Outs

At the end of the 4-6 week "Paired Work Session" cycle, please add the following section to the
bottom of the gitlab issue description, summarizing the final findings, and enumerating any proposed next steps.
You can copy & paste from the text below:

> ## Report Out Conclusions
> 
> ### Summary
> 
> 3-4 sentence summary of what work has been done over the past 4-6 weeks on this paired work session, and what conclusions were reached by
> the end of the cycle.
> 
> ## Proposed Next Steps
> 
> Propose (with a description as to why) one of the following as next steps for this Paired Work Session (to be approved by OpenTEAM Community + Secretariat)
> - [ ] Transition to fully scoped & budgeted Work Package
> - [ ] Continue in discovery cycle during next round of Paired Work Sessions
> - [ ] Close issue (other work is more important, or there is no further work necessary for this issue)