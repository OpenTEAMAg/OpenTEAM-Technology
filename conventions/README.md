# DRAFT: OpenTEAM Conventions Specification
__NOTE: This draft is still under active development and not considered stable.__

To get the ball rolling, let's just start with these basic requirements for any convention:

- Conventions should be published in [repo url].
    - It should have the following identifiers:
    - The entity and bundle it constrains (eg, "log--input")
    - The convention name or id
- The convention version number
    - It should define a set of constraints, each of which includes:
    - The field it constrains
    - The subschema that field must validate against (as JSON Schema, protobuf, or what have you)
- Maybe some info on what orgs are using this convention?
- etc...

Please submit revisions to the specification as a pull request on this document.
